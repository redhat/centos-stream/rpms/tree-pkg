#!/bin/bash

# print commands as they are executed by the shell interpreter
set -x

# run the test
./runtest.sh

# save current out put as the expected output
mv -v tree.{out,exp}
