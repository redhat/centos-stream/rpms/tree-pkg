#!/bin/bash

# exit immediately if any command returns non-zero exit code
set -e

# print commands as they are executed by the shell interpreter
set -x

# set locale
export LANG=C
export LC_ALL=C
locale

# print basic info about `tree`
rpm -q tree
command -v tree
tree --version
ldd /usr/bin/tree

# create a testing directory tree
rm -rf dir-tree
mkdir dir-tree
(
    set +x
    cd dir-tree
    for level in {1..16}; do
        subdir=subdir-l${level}
        touch {A,Z,a,z}-regular
        ln -s a-regular symlink
        ln -s $subdir dir-symlink
        ln -s .. parent-dir-symlink
        ln -s / root-dir-symlink
        ln -s invalid danglging-symlink
        mkdir -p $subdir
        cd $subdir
    done
)
tree dir-tree > tree.out
diff -u tree.{exp,out}
